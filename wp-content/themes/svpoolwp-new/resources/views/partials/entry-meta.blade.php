<div class="row subpost-meta my-2 align-items-center justify-content-between">
  <div class="col-lg-3 byline author vcard"><i class="fas fa-user"></i> <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author" class="fn">{{ get_the_author() }}</a></div>
  <div class="col-lg-3"><i class="fas fa-calendar-alt"></i> <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time></div>
  <div class="col-lg-3 cat-label">
    <i class="fas fa-folder-open"></i>
    @php
      $categories = get_the_category();
      if ( ! empty( $categories ) ) {
          echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
      }  
    @endphp
  </div>
</div>

<article @php post_class() @endphp>
  <div class="row featured-image">
    <div class="col"><?php the_post_thumbnail('lrg-img') ?></div>
  </div>
  <div class="submeta">
     @include('partials/entry-meta')
  </div>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <div class="tagmeta">
     @include('partials/tag-meta')
  </div>
</article>



<div class="col-6 col-md-3">
  <article <?php post_class(); ?> >
    <div class="post-block">
        <div class="row align-items-start justify-content-center">
          <div class="col-12 mx-auto poster-img"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_post_thumbnail('sml-img') ?></a></div>
          <div class="col-12 mx-auto cat-postmeta">
            <div class="row align-items-center justify-content-between my-2">
              <div class="col cat-postmeta-category text-left"><?php the_category( ', ' ); ?></div>
              <div class="col cat-postmeta-date text-right"><?php the_time('m-d-Y'); ?></div>
            </div>
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
          </div>
        </div>
    </div>
  </article>
</div>

<div class="row subpost-tagmeta my-4">
  <div class="col-12 post-tags-label my-2">
  <?php
    if ( is_singular() ) :
        echo get_the_tag_list(
            '<ul class="list-inline tagmeta-list"><li class="list-inline-item">',
            '</li><li class="list-inline-item">',
            '</li></ul>',
            get_queried_object_id()
        );
    endif;
  ?>
  </div>
</div>

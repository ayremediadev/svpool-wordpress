<section  class="header4 cid-qRQKyXWDas mbr-parallax-background" id="notice-top" style="position: fixed; width: 100%; z-index: 99999; height: 30px; text-align: center; 
  background: #FFD426; padding: 0;">
      <p>⚠ <a target="_blank" href="https://bitcoinsv.io/2018/11/05/bitcoin-cash-bch-november-15-2018-protocol-upgrade-notice-to-cryptocurrency-exchanges-bitcoin-cash-wallet-operators/" style="color: #000;
  text-decoration: underline; font-size: 0.9em; font-weight: bold !important; line-height: 1.8em !important;">NOVEMBER 15th BitcoinSV UPGRADE NOTICE</a></p>
    </section>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav" style="top: 30px;">
      <div class="container">
        <div class="container-fluid px-0">  

       <div class="row w-auto align-items-center justify-content-between">
        <div class="col-5 col-md col-lg order-1 order-lg-1">
          <a class="brand navbar-brand js-scroll-trigger brand-logo" href="{{ home_url('/') }}"><img src="@asset('images/svpool-logo-white.png')" class="img-fluid" alt="{{ get_bloginfo('name', 'display') }}"></a>
        </div>
        <div class="col-auto d-lg-none order-3 order-lg-4">
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
          </button>
        </div>
        <div class="col-12 col-lg-auto order-4 order-lg-2">
          @if (has_nav_menu('topmain_navigation'))
            {!! wp_nav_menu([
              'theme_location' => 'topmain_navigation',
              'container_class' => 'collapse navbar-collapse',
              'container_id' => 'navbarResponsive',
              
              'menu_class' => 'navbar-nav ml-auto',
              ]) !!}
          @endif
        </div>
        <div class="col-auto order-2 px-0 order-md-2 order-lg-3">
          <div class="navbar-nav">
            <div class="row align-items-center no-gutters">
              <div class="col-auto">
                <ul class="list-inline socialicons-header">

                  <?php // Associative array of social profiles followed by the keys of the array in a separate array. Turned into a foreach loop on Tuesday 25th September 2018. Chris Brosnan. 
                  $socialProfiles = array("twitter" => "svpoolmining", "facebook" => "satoshivisionpool", "reddit" => "r/svpool"); 
                  foreach($socialProfiles as $profile){ 
                      $keyName = array_search($profile, $socialProfiles); ?>
                      <li class="list-inline-item"><a target="_blank" href="https://<?php echo $keyName; ?>.com/<?php echo $profile; ?>" title="FolowUs on <?php echo ucfirst($keyName); ?>" class="social-block"><i class="fab fa-<?php echo $keyName; ?>"></i></a></li>
					        <?php } ?>
                
                </ul>
              </div>
              <div class="col-auto">
                <?php do_action('icl_language_selector'); ?>
              </div>
            </div>

          </div>
        </div>

       </div>

      </div>

    </div>
    </nav>

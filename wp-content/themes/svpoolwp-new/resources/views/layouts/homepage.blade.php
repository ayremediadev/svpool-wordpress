<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')

    <header class="masthead">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-lg-12 my-auto">
            
              <div class="row">
                <div class="logo-icon col-5 col-sm-5 col-md-4 col-lg-2 mx-auto mb-5 text-center">
                  <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                </div>
              </div>
              
            <div class="header-content mx-auto text-center">
              <h1 class="mb-5">{!! App::title() !!}</h1>
              <p>@php the_content() @endphp</p>
              <a href="#aboutus" class="btn btn-outline btn-xl js-scroll-trigger">Learn More</a>
            </div>
          </div>
        </div>
      </div>
    </header>

    <section class="quote" id="quote" style="padding: 100px 0 0 0;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 my-auto">
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-12">
                  @php echo get_field( 'block_1' ) @endphp
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="aboutus" id="aboutus">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 my-auto">
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-4">
                  <div class="feature-item">
                    <div class="topfeaturedimg mb-2">
                      <?php 
                        $image = get_field('first_column_image');
                        $alt = $image['alt'];
                        $size = 'sml-img';
                        $thumb = $image['sizes'][ $size ];
                        $width = $image['sizes'][ $size . '-width' ];
                        $height = $image['sizes'][ $size . '-height' ];

                        if( !empty($image) ): ?>

                          <img src="<?php echo $thumb; ?>" alt="<?php echo $image['alt']; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="img-fluid" />

                      <?php endif; ?>   
                    </div>
                    <h3>@php echo get_field( 'first_column_title' ) @endphp</h3>
                    <p class="text-muted">@php echo get_field( 'first_column_content' ) @endphp</p>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="feature-item">
                    <div class="topfeaturedimg mb-2">
                    <?php 
                        $image = get_field('second_column_image');
                        $alt = $image['alt'];
                        $size = 'sml-img';
                        $thumb = $image['sizes'][ $size ];
                        $width = $image['sizes'][ $size . '-width' ];
                        $height = $image['sizes'][ $size . '-height' ];

                        if( !empty($image) ): ?>

                          <img src="<?php echo $thumb; ?>" alt="<?php echo $image['alt']; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="img-fluid" />

                      <?php endif; ?>   
                    </div>
                    <h3>@php echo get_field( 'second_column_title' ) @endphp</h3>
                    <p class="text-muted">@php echo get_field( 'second_column_content' ) @endphp</p>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="feature-item">
                    <div class="topfeaturedimg mb-2">
                    <?php 
                        $image = get_field('first_column_image');
                        $alt = $image['alt'];
                        $size = 'sml-img';
                        $thumb = $image['sizes'][ $size ];
                        $width = $image['sizes'][ $size . '-width' ];
                        $height = $image['sizes'][ $size . '-height' ];

                        if( !empty($image) ): ?>

                          <img src="<?php echo $thumb; ?>" alt="<?php echo $image['alt']; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="img-fluid" />

                      <?php endif; ?>                         

                    </div>
                    <h3>@php echo get_field( 'third_column_title' ) @endphp</h3>
                    <p class="text-muted">@php echo get_field( 'third_column_content' ) @endphp</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    @php do_action('get_footer') @endphp
    @include('partials.footer')

  </body>

</html>
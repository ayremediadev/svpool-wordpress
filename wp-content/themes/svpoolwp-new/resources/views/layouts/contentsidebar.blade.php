<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')

    <header class="innerhead">
      <div class="container">
        <div class="row pt-5">
          <div class="col-md-10 my-5 mx-auto">
            <div class="header-content mx-auto text-center">
              <h1>{!! get_the_title() !!}</h1>
            </div>
          </div>
        </div>
      </div>
    </header>


    <div class="wrap container py-5" role="document">
      <div class="content">
        <main class="main">

          <div class="row">
            <div class="col-md-8 mx-auto">
              @yield('content')
            </div>
            <div class="col-md-4 mx-auto sidebar">
              @include('partials.sidebar')
            </div>
          </div>
          
        </main>

      </div>
    </div>



    @php do_action('get_footer') @endphp
    @include('partials.footer')

  </body>

</html>

<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body style="background: #fff !important;" @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')

    <header class="innerhead" style="width: 100%; max-width: 100%; padding: 5rem 0 0 0;">
      <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%;" />
    </header>


    <div class="wrap container py-5" role="document">
      <div class="content">
        <main class="main">
          <div class="row">
            <div class="col-12 mx-auto">
              <h1>{!! App::title() !!}<br/>
              <div id="footerTitle"></div></h1>
              @yield('content')
            </div>
          </div>
        </main>
      </div>
    </div>



    @php do_action('get_footer') @endphp
    @include('partials.footer')

  </body>

</html>

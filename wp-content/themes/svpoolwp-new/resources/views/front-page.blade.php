{{--
  Template Name: Homepage Template
--}}

@extends('layouts.homepage')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.front-page')
  @endwhile
@endsection

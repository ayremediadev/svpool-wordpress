<article <?php post_class() ?>>
  <div class="row featured-image">
    <div class="col"><?php the_post_thumbnail('lrg-img') ?></div>
  </div>
  <div class="submeta">
     <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
  <div class="entry-content">
    <?php the_content() ?>
  </div>
  <div class="tagmeta">
     <?php echo $__env->make('partials/tag-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
</article>

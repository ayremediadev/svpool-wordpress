<?php $referer = wp_get_referer(); ?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php wp_head() ?>
  <link rel="shortcut icon" type="image/x-icon" href="https://svpool.com/SVPOOL.ico">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script>
    $(function(){
      $('a[href^="https://"]').not('a[href*=svpool]').attr('target','_blank');
      $('#menu-item-56').attr('style', 'margin-left: .5em');
      $('.img-fluid').attr('style', 'margin-bottom: 1em'); 
      <?php 
      // Condition added to change copyright text depending on site language
      // Chris Brosnan - 16th October 2018
      if(ICL_LANGUAGE_CODE=='en'){

        $learn = 'Learn more'; 

      } elseif(ICL_LANGUAGE_CODE=='zh-hans') {

        $learn = '了解更多'; ?>

        $('#menu-item-3099').attr('style', 'margin-left: .5em; margin-right: .5em; border: 1px #fff solid;');

      <?php } ?>
      $('.header-content a.btn').html('<?php echo $learn; ?>');
    })(jQuery);
  </script>
  <script type="text/javascript">
    var form = document.getElementById("SVPoolContactForm");

    form.onsubmit = function() { return false; } // ensure ENTER won't cause submit

    form.submitFormBtn.onclick = function( )
    {
        this.value = "Processing";
        document.getElementById("statusMessage").innerHTML = "Thank you for your message. It has been sent."; 
        setTimeout( function() { form.submit(); }, 5000 );
    }
  </script>
  <style>
    .navbar-shrink #menu-item-2985 { border: 1px #000 solid; }
    #menu-item-2985 { border: 1px #fff solid; }
  </style>
</head>


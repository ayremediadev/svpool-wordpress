<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php wp_head() ?>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script>
    $(function(){
      $('a[href^="https://"]').not('a[href*=svpool]').attr('target','_blank');
    })(jQuery);
  </script>
  <script>
    setInterval(function() {
      var empty = false;
      var count = 0; 
      $('form > input').each(function() {
        // Count initially set at 0, increases by 1 if a required field is completed, reduces
        // by 1 if a required field is blank.Maximum value is 5 (number of required fields), minimum 
        // value is 0. 
        <?php // PHP array of all required field names, looped to check values for each in JS script 
        //$fields = array('#fullname', '#subject', '#email', '#phone'); 
        //foreach ($fields as $fi){ ?>
            count += 1;
          /*$('<?php //echo $fi; ?>').val('--None--');
          if ($('<?php //echo $fi; ?>').val() == '' ) {
            count += 1;
            empty = true;
            if(count < 0){
              count = 0;
            }
            if(count > 0 && count < 4){
              count -= 1;
            }
            $('<?php //echo $fi; ?>').siblings().css( "opacity", "1" );
          } else {
            empty = false; 
            count += 1;
            $('<?php //echo $fi; ?>').siblings().css( "opacity", "0" );
          }*/
        <?php //} ?>
        /*if (count < 4) {
          $('#submitForm').attr('disabled', 'disabled');
          $('#submitForm').addClass('disabled'); 
        } else {
          $('#submitForm').removeAttr('disabled');
          $('#submitForm').removeClass('disabled'); 
        }*/
      });
      document.getElementById("counter").innerHTML = "The counter is: " + count;
    }, 100)(jQuery);
  </script>
  <style>
    .firstRow td, .lastRow td, .selectRow td {
      display: table-cell !important;
    }
    #crmWebToEntityForm tr td:nth-of-type(1) {
      display: none; 
    }
  </style>
</head>


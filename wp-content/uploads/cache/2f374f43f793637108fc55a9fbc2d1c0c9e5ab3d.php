    <section class="contact bg-primary" id="contact">
      <div class="container">
      <?php 
        $CF_page = get_field('contact_form', 'option');
        query_posts("page_id=$CF_page->ID");
        while ( have_posts() ) : the_post()
      ?>

        <h2><?php echo the_title() ?></h2>
          <div class="row justify-content-between align-items-start">
            <div class="col-md-7 col-lg-6 my-auto mx-auto">
              <?php the_content() ?>
            </div>
          </div>
          
      <?php endwhile; wp_reset_query(); ?>




      </div>
    </section>
<footer class="content-info">
  <div class="container">

    <?php 
    // Condition added to change copyright text depending on site language
    // Chris Brosnan - 16th October 2018
    if(ICL_LANGUAGE_CODE=='en'){

      $copyr = 'All Rights Reserved';

    } elseif(ICL_LANGUAGE_CODE=='zh-hans') {

      $copyr = '版权所有';

    } ?>

    <p>&copy; <a href="<?php echo e(home_url('/')); ?>"><?php echo e(get_bloginfo('name', 'display')); ?></a> <?php echo date("Y") . '. ' . $copyr; ?>.</p>

        <?php if(has_nav_menu('footer_menu')): ?>
          <?php echo wp_nav_menu([
            'theme_location' => 'footer_menu',
            'menu_class' => 'nav justify-content-center',
            ]); ?>

        <?php endif; ?>

    <?php dynamic_sidebar('sidebar-footer') ?>
  </div>
</footer>

<?php wp_footer() ?>
  <script type="text/javascript">
    var templateUrl = '<?php echo get_theme_file_uri(); ?>';
  </script>

  <script type="text/javascript">

    (function($) {
      "use strict"; // Start of use strict

      // Closes responsive menu when a scroll trigger link is clicked
      $('.js-scroll-trigger').click(function() {
        $('.navbar-collapse').collapse('hide');
      });

      // Activate scrollspy to add active class to navbar items on scroll
      $('body').scrollspy({
        target: '#mainNav',
        offset: 54,
      });

      // Collapse Navbar
      var navbarCollapse = function() {

        // if ($("#mainNav").offset().top > 100) && ($(window).width() < 992)) {
        //   if ($("#mainNav").offset().top > 100) {
        //     $("#mainNav").addClass("navbar-shrink");
        //     $("#mainNav .brand-logo img").attr("src", "<?= App\asset_path('images/svpool-logo-black.png'); ?>"); 
        //   } else {
        //     $("#mainNav").removeClass("navbar-shrink");
        //     $("#mainNav .brand-logo img").attr("src", "<?= App\asset_path('images/svpool-logo-white.png'); ?>");
        //   }
        // }
        // else {
        //   if ($("#mainNav").offset().top > 100) {
        //     $("#mainNav").addClass("navbar-shrink");
        //     $("#mainNav .brand-logo img").attr("src", "<?= App\asset_path('images/svpool-logo-black.png'); ?>"); 
        //   } else {
        //     $("#mainNav").removeClass("navbar-shrink");
        //     $("#mainNav .brand-logo img").attr("src", "<?= App\asset_path('images/svpool-logo-white.png'); ?>");
        //   }
        // }

        if ($(window).width() < 992) {
          $("#mainNav").addClass("navbar-shrink");
          $("#mainNav .brand-logo img").attr("src", "<?= App\asset_path('images/svpool-logo-black.png'); ?>"); 
        }
        else {
          if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
            $("#mainNav .brand-logo img").attr("src", "<?= App\asset_path('images/svpool-logo-black.png'); ?>"); 
          } else {
            $("#mainNav").removeClass("navbar-shrink");
            $("#mainNav .brand-logo img").attr("src", "<?= App\asset_path('images/svpool-logo-white.png'); ?>");
          }

        }
     
      };
      // Collapse now if page is not at top
      navbarCollapse();
      // Collapse the navbar when page is scrolled
      $(window).scroll(navbarCollapse);



    })(jQuery); // End of use strict

  </script>  
  
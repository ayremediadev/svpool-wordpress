<?php $__env->startSection('content'); ?>
    <?php
    $categories = get_categories(); 

    foreach ( $categories as $category ) {
    
      $args = array(
          'cat' => $category->term_id,
          'post_type' => 'post',
          'posts_per_page' => '4',
      );

      $query = new WP_Query( $args );
      
      if ( $query->have_posts() ) { ?>
          <?php
              $category_id = get_cat_ID( $category->name );
              $category_link = get_category_link( $category_id );
          ?>
          <div class="<?php echo $category->name; ?> listing row news-block">
            <div class="col-12 mx-auto my-2">
              <div class="row align-items-center justify-content-between">
                <div class="col"><h2>Latest in <?php echo $category->name; ?></h2></div>
                <div class="col-auto"><a href="<?php echo esc_url( $category_link ); ?>" title="Display All <?php echo $category->name; ?> News">See All</a></div>
              </div>
            </div>
            
              
      
              <?php while ( $query->have_posts() ) {
      
                  $query->the_post();
                  ?>
      
                  <?php echo $__env->make('partials.archive-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      
              <?php } // end while ?>
      
          </div>
      
      <?php } // end if
      
      // Use reset to restore original query.
      wp_reset_postdata();

    }
    ?>
<?php $__env->stopSection(); ?>


    <!-- <?php echo $__env->make('partials.archive-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> -->

<?php echo $__env->make('layouts.fullpage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
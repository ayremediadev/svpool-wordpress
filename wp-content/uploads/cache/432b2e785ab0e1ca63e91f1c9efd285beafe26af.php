<?php $__env->startSection('content'); ?>


<?php 
// the query to set the posts per page to 3
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array('posts_per_page' => 16, 'paged' => $paged );
query_posts($args); ?>
<!-- the loop -->
<?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
<?php echo $__env->make('partials.archive-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endwhile; ?>
<?php echo wpse247219_custom_pagination(); ?>

<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
        <span class="sr-only">Previous</span>
      </a>
    </li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Next</span>
      </a>
    </li>
  </ul>
</nav>
<?php else : ?>
<!-- No posts found -->
<?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.archive', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
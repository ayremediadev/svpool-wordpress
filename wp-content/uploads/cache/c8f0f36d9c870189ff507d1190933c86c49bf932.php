<!doctype html>
<html <?php echo get_language_attributes(); ?>>
  <?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <body <?php body_class() ?>>
    <?php do_action('get_header') ?>
    <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <header class="innerhead">
      <div class="container">
        <div class="row pt-5">
          <div class="col-md-10 mb-md-3 mt-md-0 my-5 mx-auto">
            <div class="header-content mx-auto text-center">
              <h1>
                <?php
                  $categories = get_the_category();
                  
                  if ( ! empty( $categories ) ) {
                      echo esc_html( $categories[0]->name );   
                  }                
                ?>

              </h1>
            </div>
          </div>
        </div>
      </div>
    </header>


    <div class="wrap container py-5" role="document">
      <div class="content">
        <main class="main">
          <div class="row align-items-start justify-content-center">
            <div class="col-12 mx-auto">
              <div class="row align-items-start justify-content-center">
                <?php echo $__env->yieldContent('content'); ?>
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>

    <?php do_action('get_footer') ?>
    <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  </body>

</html>

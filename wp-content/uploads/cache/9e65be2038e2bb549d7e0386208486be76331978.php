<header class="masthead">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-lg-12 my-auto">
            
              <div class="row">
                <div class="logo-icon col-5 col-sm-5 col-md-4 col-lg-2 mx-auto mb-5 text-center">
                  <img src="<?= App\asset_path('images/svpool-icon.png'); ?>" class="img-fluid">
                </div>
              </div>
              
            <div class="header-content mx-auto text-center">
              <h1 class="mb-5"><?php echo App::title(); ?></h1>
              <p><?php the_content() ?></p>
              <a href="#aboutus" class="btn btn-outline wht btn-xl js-scroll-trigger">Learn More</a>
            </div>
          </div>
        </div>
      </div>
    </header>

    <section class="aboutus" id="aboutus">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 my-auto">
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-4">
                  <div class="feature-item">
                    <div class="topfeaturedimg mb-2">
                      <?php 
                        $image = get_field('first_column_image');
                        $alt = $image['alt'];
                        $size = 'sml-img';
                        $thumb = $image['sizes'][ $size ];
                        $width = $image['sizes'][ $size . '-width' ];
                        $height = $image['sizes'][ $size . '-height' ];

                        if( !empty($image) ): ?>

                          <img src="<?php echo $thumb; ?>" alt="<?php echo $image['alt']; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="img-fluid" />

                      <?php endif; ?>   
                    </div>
                    <h3><?php echo get_field( 'first_column_title' ) ?></h3>
                    <p class="text-muted"><?php echo get_field( 'first_column_content' ) ?></p>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="feature-item">
                    <div class="topfeaturedimg mb-2">
                    <?php 
                        $image = get_field('second_column_image');
                        $alt = $image['alt'];
                        $size = 'sml-img';
                        $thumb = $image['sizes'][ $size ];
                        $width = $image['sizes'][ $size . '-width' ];
                        $height = $image['sizes'][ $size . '-height' ];

                        if( !empty($image) ): ?>

                          <img src="<?php echo $thumb; ?>" alt="<?php echo $image['alt']; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="img-fluid" />

                      <?php endif; ?>   
                    </div>
                    <h3><?php echo get_field( 'second_column_title' ) ?></h3>
                    <p class="text-muted"><?php echo get_field( 'second_column_content' ) ?></p>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="feature-item">
                    <div class="topfeaturedimg mb-2">
                    <?php 
                        $image = get_field('first_column_image');
                        $alt = $image['alt'];
                        $size = 'sml-img';
                        $thumb = $image['sizes'][ $size ];
                        $width = $image['sizes'][ $size . '-width' ];
                        $height = $image['sizes'][ $size . '-height' ];

                        if( !empty($image) ): ?>

                          <img src="<?php echo $thumb; ?>" alt="<?php echo $image['alt']; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="img-fluid" />

                      <?php endif; ?>                         

                    </div>
                    <h3><?php echo get_field( 'third_column_title' ) ?></h3>
                    <p class="text-muted"><?php echo get_field( 'third_column_content' ) ?></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
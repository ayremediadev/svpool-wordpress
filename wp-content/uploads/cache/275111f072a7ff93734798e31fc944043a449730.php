<!doctype html>
<html <?php echo get_language_attributes(); ?>>
  <?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <body <?php body_class() ?>>
    <?php do_action('get_header') ?>
    <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <header class="innerhead">
      <div class="container">
        <div class="row pt-5">
          <div class="col-md-10 my-5 mx-auto">
            <div class="header-content mx-auto text-center">
              <h1><?php echo get_the_title(); ?></h1>
            </div>
          </div>
        </div>
      </div>
    </header>


    <div class="wrap container py-5" role="document">
      <div class="content">
        <main class="main">

          <div class="row">
            <div class="col-md-8 mx-auto">
              <?php echo $__env->yieldContent('content'); ?>
            </div>
            <div class="col-md-4 mx-auto sidebar">
              <?php echo $__env->make('partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
          </div>
          
        </main>

      </div>
    </div>



    <?php do_action('get_footer') ?>
    <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  </body>

</html>

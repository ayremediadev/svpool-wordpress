<!doctype html>
<html <?php echo get_language_attributes(); ?>>
  <?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <body style="background: #fff !important;" <?php body_class() ?>>
    <?php do_action('get_header') ?>
    <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <header class="innerhead" style="width: 100%; max-width: 100%; padding: 5rem 0 0 0;">
      <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%;" />
    </header>


    <div class="wrap container py-5" role="document">
      <div class="content">
        <main class="main">
          <div class="row">
            <div class="col-12 mx-auto">
              <h1><?php echo App::title(); ?><br/>
              <div id="footerTitle"></div></h1>
              <?php echo $__env->yieldContent('content'); ?>
            </div>
          </div>
        </main>
      </div>
    </div>



    <?php do_action('get_footer') ?>
    <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  </body>

</html>
